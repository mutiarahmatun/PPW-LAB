from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Mutia Rahmatun Husna' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 7, 19) #TODO Implement this, format (Year, Month, Date)
npm = '1706039622' # TODO Implement this
my_univ = 'Universitas Indonesia'
my_hobi = 'reading a book'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year),
    		'npm': npm, 'univ':my_univ, 'hobi':my_hobi}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0	